from msilib.schema import ListView
from urllib import request
from django.shortcuts import render

from django.contrib.auth.mixins import LoginRequiredMixin # requires login to see a class view
from django.views.generic.list import ListView

from receipts.models import Receipt

# Create your views here.


class ReceiptListView(LoginRequiredMixin, ListView):#, request):
    
    model = Receipt#.objects.filter(user=request.user)
    template_name = "receipts/list.html"
    context_object_name = "Receipt_View"

    def get_queryset(self):
        user = self.request.user
        return Receipt.objects.filter(purchaser=user)
        
