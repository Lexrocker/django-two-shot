# from django.shortcuts import render

from django.contrib.auth.forms import UserCreationForm
from django.views.generic.edit import FormView
from django.urls.base import reverse_lazy
from django.contrib.auth import authenticate, login

from django.http.response import HttpResponseRedirect

# Create your views here.


class SignupView(FormView):  # (UserCreationForm):

    form_class = UserCreationForm
    template_name = "registration/signup.html"

    def form_valid(self, form):
        
        form.save()           
        user = form.save()
        login(self.request, user, backend='django.contrib.auth.backends.ModelBackend')

        # usern = self.request.POST['username'],
        # passw = self.request.POST['password1']
        # #usern = form.cleaned_data['username'],
        # #passw = form.cleaned_data['password1']

        # #raw_pass = form.cleaned_data.get('password')
        # #raw_pass = form.cleaned_data.get('password1')

        # user = authenticate(
        #                     username=usern,
        #                     password=passw
        #                     )

        # login(self.request, user)#, backend='django.contrib.auth.backends.ModelBackend')
        return HttpResponseRedirect(reverse_lazy('home'))

